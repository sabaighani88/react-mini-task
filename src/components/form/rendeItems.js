
import React from "react";
import ItemDetail from "./itemDetail";

const RenderItems = (props) => {


  return (
    <div>
      <h2 className="ui header centered row">
        <img
          className="ui image"
          src="https://static.thenounproject.com/png/671961-200.png"
        />
        <div className="content"> Item List:</div>
      </h2>
      

      <hr />
      {props.items.map((item) => {
        
        return <ItemDetail item={item} OnDelete={props.OnDelete} OnEdit={props.OnEdit} />;
      })}
    </div>
  );
};
export default RenderItems;
