import React , {useState}from "react";
import "./form.css";
import axios from "axios";
const AddItem = (props) => {
const[itemName,SetItemName]=useState("");
const[itemCount,SetItemCount]=useState(0);

 const InsertItem=async()=>{

  
    await axios.post(
        
        "http://localhost:8000/addItem",{
            itemName:itemName,
            itemCount:itemCount
        }
    ).then((response)=>{
    console.log(response.data)
props.onAddNewItem(response.data)
    }
    
    ).catch(()=>{
    
    }) 
        }
        
    const InputNameChange=(e)=>{
 SetItemName(e.target.value)
 
    }



    const InputcountChange=(e)=>{
SetItemCount(e.target.value)
    }









  return (
    <div classname="ui  segment ">
      <div className="ui  margin  form " >
      <h2 className="ui header centered row">
        <img className="ui image" src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/06/OOjs_UI_icon_add.svg/1024px-OOjs_UI_icon_add.svg.png" />
        <div className="content"> Add New Item:</div>
      </h2>

        <div className="field">
          <label className="ui label grey">item name</label>
          <input
            className="ui input"
            type="text"
            placeholder="item name"
            pattern="[A-Za-z]{1,20}"
            title="The input should only contain alphabet and length less than 20  "
            onChange={(e)=>SetItemName(e.target.value)}
            required
          ></input>
        </div>
        <div className="field">
          <label className="ui label grey">item count</label>
          <input
            className="ui input"
            type="number"
            min="0"
            onChange={e=>SetItemCount(e.target.value)}
            required
            title="fill item count"
            placeholder="item count"
          ></input>
        </div>

        <button class="ui animated button" tabindex="1"  onClick={(e)=>InsertItem(e)}>
          <div class="visible content">add item</div>
          <div class="hidden content">
            <i class="right save icon"></i>
          </div>
        </button>
      </div>
    </div>
  );
};
export default AddItem;
