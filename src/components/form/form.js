import AddItem from "./addItem";
import RenderItems from "./rendeItems";
import axios from "axios";
import React, { useState, useEffect } from "react";

const Form = () => {
  const [items, SetItems] = useState([]);
  useEffect(() => {
    ReadItems();
  }, []);

  const OnDelete =async (deletedItem) => {

    const data={
      id: deletedItem._id,
      itemName: deletedItem.itemName,
      itemCount: deletedItem.itemCount,
    }
    console.log(items)
console.log(data)
console.log(JSON.stringify(data))
    await axios
      .post("http://localhost:8000/deleteItem", {
        id: deletedItem._id,
        itemName: deletedItem.itemName,
        itemCount: deletedItem.itemCount,
      })
      .then((response) => {
        console.log(response.data);
     
      })
      .catch(() => {});
  };
  const OnEdit = async(editedItem) => {
    await axios
      .put("http://localhost:8000/editeItem", {
        id:editedItem._id,
        itemName: editedItem.itemName,
        itemCount: editedItem.itemCount,
      })
      .then((response) => {
        console.log(response.data);
       
      })
      .catch(() => {});
  };
  const ReadItems = async () => {
    await axios
      .get("http://localhost:8000/getItems")
      .then((response) => {
        SetItems(response.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const onAddNewItem = (newItem) => {
    SetItems((oldItems) => [newItem, ...oldItems]);
  };

  return (
    <div>
      <AddItem onAddNewItem={onAddNewItem} />
      <RenderItems items={items} OnDelete={OnDelete} OnEdit={OnEdit} />
    </div>
  );
};
export default Form;
