import React from 'react'

const ItemDetail=(props)=>{

  
    return(
      
        <div className="ui form">
        <div className="three fields">
          <div className="field">
            <label>item name</label>
            <input defaultValue={props.item.itemName} onChange={(e)=>props.item.itemName=e.target.value}></input>
          </div>
          <div className="field">
            <label>item count</label>
            <input defaultValue={props.item.itemCount} onChange={(e)=>props.item.itemCount=e.target.value}></input>
          </div>
          <div className="field ">
          <button class="ui animated button " tabindex="0" onClick={()=>props.OnEdit(props.item)}>
          <div class="visible content">edit item</div>
          <div class="hidden content">
            <i class="right edit icon"></i>
          </div>
        </button>
        <button class="ui animated button " tabindex="0" onClick={()=>props.OnDelete(props.item)}>
          <div class="visible content">delete item</div>
          <div class="hidden content">
            <i class="right trash icon"></i>
          </div>
        </button>
          </div>
        </div>
        <hr/>
      </div>
    )
}
export default ItemDetail;